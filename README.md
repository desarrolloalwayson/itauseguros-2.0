## Clonar Repositorio

Requisitos Previos

- *GIT (independiente del sistema operativo).
- Terminal capaz de ejecutar los comandos (GIT Bash, CMDER, POWERSHELL, BASH WSL).


Para clonar el repositorio solo basta que sigas los pasos a continuacion.

1. Veras un boton en el apartado Fuente de este repositorio, llamado "clonar". Presionalo.
2. Dentro de esta deberas seleccionar el modo de clonacion para el repo (HTTPS / SSH), para esta ocasion se utilizara HTTPS
3. Presiona el icono de copia o copia todo el texto que indica.
4. Inicia el terminal(CLI) de tu preferencia.
5. Dentro del terminal dirigete a tu directorio de trabajo o pruebas.
6. Pega la linea completa del comando copiado en bitbucket.
7. Dirigete al directorio creado por la clonacion del repositorio.
8. Ejecuta el siguiente comando "Composer Install". Esto comenzara a instalar las dependencias del proyecto.
9. Una vez tengas tu repositorio a punto puedes empezar a trabajar creando ramas locales independientes.

